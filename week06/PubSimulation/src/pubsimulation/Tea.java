/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pubsimulation;

/**
 *
 * @author jpkilbane
 */
public  class Tea extends hotDrink{
    
    @Override
    public final void makeDrink(){
    this.boilWater();
    this.brew();
    this.pourResult();
    this.extras();
    
    }
    
    
    @Override
    public final void brew(){
    System.out.println("Brewing Tea");
    }
    
    @Override
    public final void extras(){
    System.out.println("Adding lemon");
    }
    
}
