/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pubsimulation;

/**
 *
 * @author jpkilbane
 */
public class BottledDrink extends Drink{
    
    
    public void prepareDrink(){}
    
    public void openBottle(){
    System.out.println("Opening bottle");
    }
    
    
    @Override
    public final void makeDrink(){
    this.openBottle();
    this.pourDrink();
    
    }
}
