/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pubsimulation;

/**
 *
 * @author jpkilbane
 */
public abstract class Drink implements IDrink {
   
    public void serveDrink(){
    System.out.println("Serving");
    }
    
    public void pourDrink(){
    System.out.println("Pouring Drink");
    
    }
    
    public Drink(){
    this.makeDrink();
    this.serveDrink();
    
    }
}
