#include "customer.h"
//constructor
Customer::Customer(string _firstName, string _lastName, string _phone, string _email)
	: firstName(_firstName), lastName(_lastName), phone(_phone), email(_email)
{
	SetFirstName(_firstName), SetLastName(_lastName), SetPhone(_phone), SetEmail(_email);
}
//Gets
string Customer::GetFirstName() { return firstName; }
string Customer::GetLastName() { return lastName; }
string Customer::GetPhone() { return phone; }
string Customer::GetEmail() { return email; }
//sets
void Customer::SetFirstName(string _firstName) { firstName = _firstName; }
void Customer::SetLastName(string _lastName) { lastName = _lastName; }
void Customer::SetPhone(string _phone) { phone = _phone; }
void Customer::SetEmail(string _email) { email = _email; }
void Customer::Display()
{
	//output
	cout << "First name: " << GetFirstName() << endl;
	cout << "Last name: " << GetLastName() << endl;
	cout << "Phone: " << GetPhone() << endl;
	cout << "Email: " << GetEmail() << endl;
	
}
