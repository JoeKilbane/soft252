#include "order.h"
#include "paper.h"
#include "poster.h"
#include "customer.h"
#include <iomanip>
//constructor
Order::Order(Customer _customer, Poster _poster, Paper _paper)
	: customer(_customer), paper(_paper), poster(_poster)
{
	SetCustomer(_customer), SetPaper(_paper), SetPoster(_poster);
}
//Gets
Customer Order::GetCustomer() { return customer; }
Paper Order::GetPaper() { return paper; }
Poster Order::GetPoster() { return poster; }


double Order::GetOriginalCost(Poster _poster, Paper _paper)
{ 
	//calculation
	double originalCost;
	double extraCost;
	// minimum order quantity
	originalCost = _poster.GetSize() * (10 * .03);
	//minimum cost passed in
	originalCost = originalCost + _poster.GetSize() * (paper.GetExtraQuantity() * 0.0075);
	originalCost = originalCost + (paper.GetQuantity() * paper.GetPrice());
	return originalCost;
}
//sets
void Order::SetCustomer(Customer _customer) { customer = _customer; }
void Order::SetPaper(Paper _paper) { paper = _paper; }
void Order::SetPoster(Poster _poster) { poster = _poster; }

void Order::Display(Poster _poster, Paper _paper)
{
	//outputs
	paper.Display();
	poster.Display();
	customer.Display();
	cout << "Original Cost: " << std::fixed << std::setprecision(2) << GetOriginalCost(_poster, _paper) << endl;

}
