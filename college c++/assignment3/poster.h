#pragma once
#include <iostream>
#include <string>
using std::cin;
using std::cout;
using std::endl;
using std::string;

#ifndef POSTER_H
#define POSTER_H
class Poster
{
private:
	double length;
	double width;

public:
	//constructor
	Poster(double, double);
	//gets
		double GetLength();
	    double GetWidth();
		double GetSize();
		//sets
		void SetLength(double);
		void SetWidth(double);
		void Display();


};
#endif