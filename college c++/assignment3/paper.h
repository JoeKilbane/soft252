#pragma once
#include <iostream>
#include <string>
using std::cin;
using std::cout;
using std::endl;
using std::string;

#ifndef PAPER_H
#define PAPER_H
class Paper
{
private:
	string type;
	int quantity;

public:
	//constructor
	Paper(string, int);
	// gets
	string GetType(string type);
	double GetPrice();
	int GetQuantity();
	int GetExtraQuantity();
	//sets
	void SetType(string);
	void SetQuantity(int);


	void Display();
};
#endif