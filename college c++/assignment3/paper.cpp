#include "paper.h"
	//constructor
Paper::Paper(string _type, int _quantity)
	: type(_type), quantity(_quantity)
{
	SetType(_type),  SetQuantity(_quantity);
}
string Paper::GetType(string typeCheck) { 
	
	//paper check

		if (typeCheck == "1") {
			type = "115gm";

		}
		else if (typeCheck == "2") {
			type = "135gm";
		}
		else if (typeCheck == "3") {
			type = "PVC";
		}
	
	

	return type;

}
double Paper::GetPrice() { 
	//paper value to price
	double price;
	 if (type == "115gm") {
		price = 0.0;

	}
	else if (type == "135gm") {
		price = 0.05;
	}
	else if (type == "PVC") {
		price = 0.35;
	}

	return price;

}
int Paper::GetQuantity() { return quantity; }
int Paper::GetExtraQuantity() {
	
	return GetQuantity()-10;
}

void Paper::SetType(string _type) { type = _type; }

void Paper::SetQuantity(int _quantity) { quantity = _quantity; }


void Paper::Display()
{
	//output

	cout << "Paper type:     " << GetType(type) << endl;
	cout << "Price :         " << GetPrice() << endl;
	cout << "Quantity:       " << GetQuantity() << endl;
	cout << "Extra Quantity: " << GetExtraQuantity() << endl;



}