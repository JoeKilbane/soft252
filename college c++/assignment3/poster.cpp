#include "poster.h"
//contructor
Poster::Poster(double _length, double _width)
	: length(_length), width(_width)
{
	SetLength(_length), SetWidth(_width);
}
//gets
double Poster::GetLength() { return length; }
double Poster::GetWidth() { return width; }
double Poster::GetSize() { return length + width; }

// sets
void Poster::SetLength(double _length) { length = _length; }
void Poster::SetWidth(double _width) { width = _width; }


void Poster::Display()
{
	//output
	cout << "length: " << GetLength() << endl;
	cout << "Width:  " << GetWidth() << endl;
	
}