#pragma once
#include <iostream>
#include <string>
using std::cin;
using std::cout;
using std::endl;
using std::string;

#ifndef CUSTOMER_H
#define CUSTOMER_H
class Customer
{
private:
	string firstName;
	string lastName;
	string phone;
	string email;

public:
	//constructor
	Customer(string, string, string, string);
	//gets
	string GetFirstName();
	string GetLastName();
	string GetPhone();
	string GetEmail();
	//sets
	void SetFirstName(string);
	void SetLastName(string);
	void SetPhone(string);
	void SetEmail(string);
	void Display();
};
#endif
