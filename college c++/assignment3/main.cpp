//declare functions to hold separate menu content
void MainMenu();
void CreateOrder();
void HelpMenu();
void Exit();

#include "customer.h"
#include "poster.h"
#include "paper.h"
#include "order.h"
#include <iostream>
#include <string>
#include <iomanip>
#include <stdlib.h>
using std::cin;
using std::cout;
using std::endl;
using std::string;

int main(){
	MainMenu();
	
	
	cin.ignore(2);
	return 0;

}

void MainMenu()
{
	//this clears the cli, making it more presentable
	system("CLS");
	string userChoice;
	//This is the main menu, the user is given a choice of where to go in the program
	cout << "Welcome to the Main Menu" << endl;
	cout << "Please select an option: " << endl;
	cout << "[1] Create Order" << endl << "[2] Help Menu" << endl << "[3] Exit" << endl;
	cin >> userChoice;
	cout << "Your choice: " << userChoice << endl;

	if (userChoice == "1") {
		CreateOrder();
	}
	else if (userChoice == "2") {
		HelpMenu();
	}
	else if (userChoice == "3"){
		Exit();
	}
	else {
		//if the entry is incorrect then the main menu is called
		cout << "Incorrect entry, enter 1, 2 or 3" << endl;
		cout << endl;
		cin.ignore(2);
		//takes user to main menu
		MainMenu();

	
	}
}

void CreateOrder()
{
	//declare variables
	string typeCheck;
	string firstName;
	string lastName;
	string phone;
	string email;
	int quantity;
	double length;
	double width;
	string userChoice;

	system("CLS");
	cout << "Poster R US" << endl;


	// user inputs
	cout << "Please enter your First Name: " << endl;
	cin >> firstName;

	cout << "Please enter your Last Name: " << endl;
	cin >> lastName;

	cout << "Please enter your Phone Number: " << endl;
	cin >> phone;

	cout << "Please enter your email: " << endl;
	cin >> email;

	cout << "Enter paper type [1] 115gm, [2] 135gm [3] PVC:" << endl;
	cin >> typeCheck;

	cout << "Please enter the length: " << endl;
	cin >> length;

	cout << "Please enter the width: " << endl;
	cin >> width;

	cout << "Please enter the quantity: " << endl;
	cin >> quantity;
	//This checks the quantity is over 10 the builds the objects
	if (quantity < 10) {
		cout << "Minimum order is 10 " << endl;

	}
	else {
		Customer c1 = Customer(firstName, lastName, phone, email);
		Poster p1 = Poster(length, width);

		Paper pp1 = Paper(typeCheck, quantity);

		Order o1 = Order(c1, p1, pp1);

		o1.Display(p1, pp1);

	}

	cout << "Please select an option: " << endl;
	cout << "[1] Create another order" << endl << "[2] Main Menu" << endl << endl;
	cin >> userChoice;
	cout << "Your choice: " << userChoice << endl;

	if (userChoice == "1") {
		//takes user to the create order
		CreateOrder();
	}
	else if (userChoice == "2") {
		//takes user to main menu
		MainMenu();
	}
	else {
		//if the entry is incorrect then the main menu is called
		cout << "Incorrect entry, enter 1, 2 or 3" << endl;
		cout << endl;
		cin.ignore(2);
		MainMenu();
	}
	


}
void HelpMenu()
{
	//Help menu, this menu is designed to help the user
	system("CLS");
	string userChoice;
	cout << "Welcome to the help menu" << endl;
	cout << "There is a minimum order quantity of 10." << endl <<
		"Your options will be presented as you progress through the program." << endl <<
	"When presented with an option enter an appropriate entry, this will generally be either text or a number" << endl;

	cout << "Please select an option: " << endl;
	cout << "[1] Main Menu" << endl << "[2] Exit" << endl << endl;
	cin >> userChoice;
	cout << "Your choice: " << userChoice << endl;

	if (userChoice == "1") {
		//takes user to main menu
		MainMenu();
	}
	else if (userChoice == "2") {
		//exits the program
		Exit();
	}
	else {
		cout << "Incorrect entry, enter 1, 2 or 3" << endl;
		cin.ignore(2);
		MainMenu();
	}

}
void Exit()
{
	//this exits the program
	exit(0);
}