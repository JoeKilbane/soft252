#pragma once
#include "customer.h"
#include "poster.h"
#include "paper.h"
#include <iostream>
#include <string>
using std::cin;
using std::cout;
using std::endl;
using std::string;

#ifndef ORDER_H
#define ORDER_H
class Order
{
private: 

	Customer customer;
	Poster poster;
	Paper paper;

public:
	//constructor
	Order(Customer, Poster, Paper);
	//gets
	 Paper GetPaper();
	 Poster GetPoster();
	 Customer GetCustomer();
	 double GetOriginalCost(Poster poster, Paper paper);

	 //sets
	 void SetCustomer(Customer);
	 void SetPaper(Paper);
	 void SetPoster(Poster);

	 void Display(Poster, Paper);



};
#endif
