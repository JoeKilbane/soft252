/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stocktracker;

import stocktracker.stockdatamodel.AnObserver;
import stocktracker.stockdatamodel.PhysicalStockItem;
import stocktracker.stockdatamodel.ServiceStockItem;
import stocktracker.stockdatamodel.StockItem;
import stocktracker.stockdatamodel.StockType;
import utilities.IObserver;

/**
 *
 * @author jpkilbane
 */
public class StockTracker {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
     /*   
        //Create a physical stock item object and a service stock item object
        PhysicalStockItem objPhysicalItem =
                new PhysicalStockItem("Snuff: A diskworld book by Terry Prachett", 100);
        ServiceStockItem objVirtualItem=
                new ServiceStockItem("Wintersmith: A diskworld eBook by Terry Prachett");
        
        //Test the behaviour of the physical stock item
        String strMessage = objPhysicalItem.getName()
                +", Is in stock= " +objPhysicalItem.isInStock()
                +", Qty in stock: " +objPhysicalItem.getQuantityInStock();
        System.out.println(strMessage);
        
           strMessage = objVirtualItem.getName()
                +", Is in stock= " +objVirtualItem.isInStock()
                +", Qty in stock: " +objVirtualItem.getQuantityInStock();
        System.out.println(strMessage);
*/
     System.out.println("Creating stock items");
     //StockItem objtemitem1= new PhysicalStockItem("Starcraft Manual");
     StockItem objPhysical= new PhysicalStockItem("Halo 3", 100);
     StockItem objService= new ServiceStockItem("Delivery");
     System.out.println("Stock Items created");
     System.out.println("Creating an observer and registering with stock items");
     IObserver observer = new AnObserver();
     objPhysical.registerObserver(observer);
     objService.registerObserver(observer);
     System.out.println("Observer created and registered with stock items");
     System.out.println("Changing physical stock items quantity in stock");
     objPhysical.setQuantityInStock(99);
     System.out.println("Setting delivery service stock items selling price..");
     objService.setSellingPrice(5.00);
     
     
     
     if(objPhysical.getItemType() == StockType.PHYISCALITEM){
     System.out.println("Item 1 is a PHYSICAL stock item");
     }else{
         System.out.println("Item 1 is a SERVICE stock item");
     }
     
     /*if(objTestItem2.getItemType() == StockType.PHYISCALITEM){
     System.out.println("Item 2 is a PHYSICAL stock item");
     }else{
         System.out.println("Item 2 is a SERVICE stock item");
     }
*/
     if(objService.getItemType() == StockType.PHYISCALITEM){
     System.out.println("Item 3 is a PHYSICAL stock item");
     }else{
         System.out.println("Item 3 is a SERVICE stock item");
     }
     
     }
     
    }
    

