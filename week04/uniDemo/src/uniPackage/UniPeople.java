/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uniPackage;

/**
 *
 * @author jpkilbane
 */
public class UniPeople {
    protected Integer id;
    protected course course;
    protected String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public course getCourse() {
        return course;
    }

    public void setCourse(course course) {
        this.course = course;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public UniPeople(int id, String name){
    this.name=name;
    this.id=id;
    }
    
    
    
}
