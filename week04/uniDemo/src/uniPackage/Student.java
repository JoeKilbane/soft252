/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uniPackage;

/**
 *
 * @author jpkilbane
 */
public class Student extends UniPeople {
    
    public Student(int id, String name){
    super(id, name);
    this.setId(id);
    this.setName(name);
  
}
    public void attendClass(String name, String code, String room){
        System.out.println(name + " is attending "+code+ " in room " + room);
        
    }
    public void doCourseWork(String name, String coursework){
        if(coursework!=null)
        System.out.println(name+" is doing course work" +coursework);
    } 
}