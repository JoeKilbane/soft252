/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uniPackage;

/**
 *
 * @author jpkilbane
 */
public class Lecturer extends UniPeople implements ITeach {
    
    public Lecturer(int id, String name){
    super(id, name);
    this.setId(id);
    this.setName(name);
    
    }
    public void setCourseWork(String courseWork){
      
       
//Set the coursework of the course that the lecturer is teaching
      this.setCourseWork(courseWork);
        
    }
    
    public void teach(String name, String code, String room){
        
        System.out.println(name + "is teaching "+ code + "in room "+room);
        
    }
}
