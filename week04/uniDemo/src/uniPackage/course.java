/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uniPackage;

/**
 *
 * @author jpkilbane
 */
public class course {
    protected String code = "UNKNOWN";
    protected Lecturer teacher;
    protected String coursework = "UNKOWN";
    protected String room ="UNKOWN";

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Lecturer getTeacher() {
        return teacher;
    }

    public void setTeacher(Lecturer teacher) {
        this.teacher = teacher;
    }

    public String getCoursework() {
        return coursework;
    }

    public void setCoursework(String coursework) {
        this.coursework = coursework;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }
    
    public void course (String room, String code){
    this.room=room;
    this.code=code;
    }
    
}
