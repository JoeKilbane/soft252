/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stocktracker;

import stocktracker.stockdatamodel.PhysicalStockItem;
import stocktracker.stockdatamodel.ServiceStockItem;

/**
 *
 * @author jpkilbane
 */
public class StockTracker {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        //Create a physical stock item object and a service stock item object
        PhysicalStockItem objPhysicalItem =
                new PhysicalStockItem("Snuff: A diskworld book by Terry Prachett", 100);
        ServiceStockItem objVirtualItem=
                new ServiceStockItem("Wintersmith: A diskworld eBook by Terry Prachett");
        
        //Test the behaviour of the physical stock item
        String strMessage = objPhysicalItem.getName()
                +", Is in stock= " +objPhysicalItem.isInStock()
                +", Qty in stock: " +objPhysicalItem.getQuantityInStock();
        System.out.println(strMessage);
        
           strMessage = objVirtualItem.getName()
                +", Is in stock= " +objVirtualItem.isInStock()
                +", Qty in stock: " +objVirtualItem.getQuantityInStock();
        System.out.println(strMessage);
    }
    
}
