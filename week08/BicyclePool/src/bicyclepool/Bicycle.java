/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bicyclepool;
import java.util.logging.Logger;
/**
 *
 * @author jpkilbane
 */
public abstract class Bicycle {
    String description = "Bicycle";
    public String getDescription(){
    return description;
    }
    
    public void setDescription(String desc){
    this.description=desc;    
    }
    public abstract double cost();
}
