/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bicyclepool;

/**
 *
 * @author jpkilbane
 */
public class BackLight extends BicylceDecorator {
    public BackLight (Bicycle bicycle){
    super (bicycle);
    }
    
    @Override
    public String getDescription(){
    return ", Back Light";
    }
    @Override
    public double cost(){
    return bicycle.cost()+1.25;
    }
}