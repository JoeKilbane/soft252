/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bicyclepool;

import java.util.Random;
import java.util.Scanner;


/**
 *
 * @author jpkilbane
 */
public class BicyclePool {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
      
        
        String bikeTypes[] ={"Mountain Bike","Folding Bike", "Road Bike","Touring Bike"};
           String[] Hired = new String[20];
        double RunningCost=0;
       
        //Generates an array of types of bicycles
        Bicycle[] bikes = new Bicycle[20];
        Random random= new Random();
        for(int i =0; i<20; i++){
            bikes[i]=new bike();
           int n = random.nextInt(4)+0;
       bikes[i].setDescription(bikeTypes[n]);
       Hired[i]=bikes[i].getDescription();
        }
        
        Boolean HireHelmet=false;
        Boolean HirePump=false;
        Boolean HireFront=false;
        Boolean HireBack=false;
        String Hire="";
     //add in while loop from here
     do{
     
        Scanner scanner = new Scanner(System.in);
        System.out.println("User ID?");
        String staffID = scanner.nextLine();
        
        
        System.out.println("Type of bike?");
        String bikeToHire = scanner.nextLine();
        
        
        System.out.println("Need a helmet?");
        String helmetHire = scanner.nextLine();
        if("Yes".equals(helmetHire)){HireHelmet=true;}
        
        System.out.println("Need a pump?");
        String pumpHire = scanner.nextLine();
        if("Yes".equals(pumpHire)){HirePump=true;}
        
        
        System.out.println("Need a front light?");
        String frontHire = scanner.nextLine();
         if("Yes".equals(frontHire)){HireFront=true;}
         
        System.out.println("Need a back light?");
        String backHire = scanner.nextLine();
         if("Yes".equals(backHire)){HireBack=true;}
     
     
  
     String Option1="";
     String Option2="";
     String Option3="";
     String Option4="";
     
     for (int j=0; j<20; j++ ){     
     if(bikes[j].getDescription().equals(bikeToHire)){
    Bicycle H =(bikes[j]);
    H =(bikes[j]);
    RunningCost=H.cost();
     if(HireHelmet){ 
        Bicycle Helmet =new Helmet(bikes[j]);
         RunningCost=RunningCost+Helmet.cost()-10;
         Option1=Helmet.getDescription();
     }
      if(HirePump){ 
        Bicycle Pump =new HandPump (bikes[j]);
         RunningCost=RunningCost+Pump.cost()-10;
         Option2=Pump.getDescription();
     }
      if(HireFront){ 
         Bicycle Front =new FrontLight (bikes[j]);
         RunningCost=RunningCost+Front.cost()-10;
         Option3=Front.getDescription();
     }
      if(HireBack){ 
         Bicycle Back =new BackLight (bikes[j]);
         RunningCost=RunningCost+Back.cost()-10;
         Option4=Back.getDescription();
     }
      
     Hired[j]=(H.getDescription()+" "+Option1+""+""+Option2+""+""+Option3+""+""+Option4+""+"Hired to: "+staffID);
     System.out.println(H.getDescription()+" "+Option1+""+""+Option2+""+""+Option3+""+""+Option4+""+": "+(RunningCost));    
     System.out.println("Bike "+ j+ " Registered to staff id: " +staffID); 
     //Bike is nulled, cant be hired
     
           break;
      
     }
     }
     
       System.out.println("Search For Bike with bike ID?");
        int bikeInfo = scanner.nextInt();
        System.out.println(Hired[bikeInfo]);
        
      System.out.println("Bikes in pool: ");
        for (int z=0; z<20; z++ ){
        System.out.println(Hired[z]);
        }
       
         System.out.println("Hire another? ");
          Hire = scanner.nextLine();
         
     }while("Yes".equals(Hire));    
    }
    
}
