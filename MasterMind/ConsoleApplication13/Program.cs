﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterMind
{
    class MasterMind
    {

        //MasterMind program By Joseph Kilbane
        //This program has 4 main methods to break up the logic of the program, this was decided during planning. 
        //The program asks the user for all the parameters including the number of guesses, instead of a fixed
        // value of 8 as this could prove hard to crack if you have a very large range or number of positions.





        //delcared globally so isnt reset on new loop.
        int temp = 0;

        //This method asks for the parameters of the program and the majority of the arrays are declared here as this is only accessed once during a game.
        public void RunProgram()
        {


            int guessCounter = 0;
            int positions;
            int guesses;
            int range;
            temp = 0;

            //Simple try catch to stop exceptions
            try
            {
                Console.Write("Please enter the range you wish to have: ");
                range = Convert.ToInt32(Console.ReadLine());

                Console.Write("Please enter the number of positions you wish to have: ");
                positions = Convert.ToInt32(Console.ReadLine());

                Console.Write("Please enter the number of guesses you wish to have: ");
                guesses = Convert.ToInt32(Console.ReadLine());



                // arrays for the main areas of the program
                int[] code = new int[positions];
                GenerateRandom(positions, range, code, guesses);
                string[] prevScores = new string[guesses];
                //guess array is 2d, this allowed simplier logic when accessing the correct scores.
                int[,] guessArray = new int[positions, guesses];
                UserInput(positions, code, guessArray, guessCounter, guesses, prevScores);
            }
            catch (FormatException x) { Console.WriteLine("Please enter a number"); }

        }
        public int[] GenerateRandom(int positions, int range, int[] code, int guesses)
        {
            //This method is used to create the random code from the range set by the user.

            Random rnd = new Random();

            //Generate random Numbers
            for (int i = 0; i < positions; i++)
            {
                int number = rnd.Next(1, range + 1);//+1 to include the upper boundary
                code[i] = number;
            }

            return (code);
        }

        public void UserInput(int positions, int[] code, int[,] guessArray, int guessCounter, int guesses, string[] prevScores)
        {
            //User input area, user is asked for their guess one digit at a time and stores each int
            // in its own space in the array. This method therefore didnt need to use split.

            //Keeping the program simple I choose this as my input method, eg one at a time.


            try
            {
                int tempInput;
                int pos = 1;
                Console.Write("Please enter one number at a time" + "\n");
                for (int i = 0; i < positions; i++)
                {

                    Console.Write("Please enter your number at position " + pos + ": ");

                    tempInput = Convert.ToInt32(Console.ReadLine());

                    //Assigning set value
                    guessArray[i, guessCounter] = tempInput;
                    pos++;
                }
            }
            catch (FormatException e) { Console.WriteLine("Please enter a number"); }
            //Calls the final method with all previous parameters
            evaulate(guessArray, positions, code, guessCounter, guesses, prevScores);
        }


        //This method is where the comparisions are done, taking in all previous values they are first checked for a black score then
        // the remaining scores are put in a seperate array then compared to the code spares to check for whites.
        // If there is a white match the value is overwritten in the array to prevent a double score.
        public void evaulate(int[,] guessArray, int positions, int[] code, int guessCounter, int guesses, string[] prevScores)
        {
            //declared here so they reset on new guesses
            int[] codeSpares = new int[positions];
            int[] whiteArray = new int[positions];
            int black = 0;
            int white = 0;
            char playAgain;
            int prevGuesses = 0;
            //Checks for a match for a black
            for (int i = 0; i < positions; i++)
            {
                if (code[i] == guessArray[i, guessCounter])
                {
                    black++;

                    whiteArray[i] = 99;//This could be anynumber above the range, stops white errors
                }
                else
                { //If not a match, the code and guesses are put into a new array to be compared for whites later.
                    whiteArray[i] = guessArray[i, guessCounter];
                    codeSpares[i] = code[i];
                }

            }
            //nested for to check for whites
            for (int j = 0; j < positions; j++)
            {
                for (int x = 0; x < positions; x++)
                {
                    if (whiteArray[j] == codeSpares[x])
                    {
                        codeSpares[x] = 0;//to null the postion, stops double count.

                        white++;

                    }
                }
            }
            //Saves the score from the current guess, array save position then increased.
            prevScores[temp] = "Black: " + black + " White: " + white;
            temp++;

            //For debug purposes only
            /*  for (int i = 0; i < positions; i++)
              {
                  Console.WriteLine("\r\n"+"Code: " + code[i] + "--- Guessed: " + guessArray[i, guessCounter]);
              }
              */
            Console.WriteLine("\r\n" + "You have: " + black + " Black " + " And: " + (white) + " White" + "\r\n");

            //Win condition
            if (black == positions)
            {
                Console.Write("Code Cracked!");

                Console.Write("Play Again? (y/n): ");
                playAgain = Convert.ToChar(Console.ReadLine());
                if (playAgain == 'y')
                {
                    RunProgram();
                }
                else
                {
                    Environment.Exit(0);
                }

            }


            // This outputs previous scores and guesses.
            if (guessCounter >= 1)
            {
                int j = 0;
                Console.WriteLine("Previous Guesses: ");

                //Nested for in a while ensures all is checked, a set amount.
                while (prevGuesses <= guessCounter)
                {

                    for (int i = 0; i < positions; i++)
                    {
                        Console.Write(guessArray[i, (prevGuesses)]);

                        if (i == (positions - 1))
                        {
                            Console.Write(" " + prevScores[j]);
                            j++;
                        }

                    }
                    prevGuesses++;
                    Console.Write("\n");
                }

            }

            //This is before the code uncracked as it starts out as guess=0, this increment initally makes it the
            //first guess so an exception error doesn't occur.
            guessCounter++;
            //Checks if they are out of guesses, if so shows the code
            //Either quits or play again
            if (guessCounter == guesses)
            {
                Console.WriteLine("Code Uncracked! Here it is: ");
                for (int i = 0; i < positions; i++) { Console.Write(code[i]); }
                Console.WriteLine("\n");
                Console.WriteLine("Play Again? (y/n): ");
                playAgain = Convert.ToChar(Console.ReadLine());
                if (playAgain == 'y')
                {
                    RunProgram();
                }
                else
                {
                    Environment.Exit(0);
                }
            }
            //If they are not out of guesses, go back to user input.
            UserInput(positions, code, guessArray, guessCounter, guesses, prevScores);

        }

        static void Main(string[] args)
        {
            MasterMind mastermind = new MasterMind();

            mastermind.RunProgram();
            Console.ReadKey();
        }
    }
}
