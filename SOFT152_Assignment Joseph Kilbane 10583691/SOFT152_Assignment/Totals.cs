﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Threading.Tasks;

namespace SOFT152_Assignment
{
    class Totals
    {
        //These have to be public or there are protection access issues
        //This could be a bug in the class use
        public int totalInvoices;
        public double avSpend;
        public int totalQuantity;
        public double spend;
        public int uniqueCustomerAmount;
        public double averageCustomerSpend;

        //Constructor with all properties, this was not referenced.
        public Totals(int TotalInvoices, double AvSpend, int TotalQuantity, double Spend,  int UniqueCustomerAmount, double AverageCustomerSpend)
        {
            totalInvoices = TotalInvoices;
            avSpend = AvSpend;
            totalQuantity = TotalQuantity;
            spend = Spend;
           
            uniqueCustomerAmount = UniqueCustomerAmount;
            averageCustomerSpend = AverageCustomerSpend;

        }
        //This constrcutor is used to add specific values from a method to the list
        public Totals(int TotalInvoices, double AvSpend, int TotalQuantity, double Spend)
        {
            totalInvoices = TotalInvoices;
            avSpend = AvSpend;
            totalQuantity = TotalQuantity;
            spend = Spend;
          
            uniqueCustomerAmount = UniqueCustomerAmount;
            averageCustomerSpend = AverageCustomerSpend;

        }

        //This constrcutor is used to add specific values from a method to the list
        public Totals( int UniqueCustomerAmount, double AverageCustomerSpend)
        {
            totalInvoices = TotalInvoices;
            avSpend = AvSpend;
            totalQuantity = TotalQuantity;
            spend = Spend;

        
            uniqueCustomerAmount = UniqueCustomerAmount;
            averageCustomerSpend = AverageCustomerSpend;
        }
      
        //Both gets and sets are needed.
        public int TotalInvoices { set; get; }
        public double AvSpend { set; get; }
        public int TotalQuantity { set; get; }
        public double Spend { set; get; }
       
        public int UniqueCustomerAmount { set {uniqueCustomerAmount=value; } get {return uniqueCustomerAmount; } }
        public double AverageCustomerSpend { set {averageCustomerSpend=value; } get { return averageCustomerSpend; ; } }


    }
}
