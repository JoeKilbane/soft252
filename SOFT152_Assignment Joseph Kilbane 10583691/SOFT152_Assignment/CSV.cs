﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SOFT152_Assignment
{
    public partial class CSV : Form
    {

        //  Delcaring object lists
        private List<Record> recordList;
        private List<Totals> totals;

      
        public CSV()
        {
            InitializeComponent();

            recordList = new List<Record>();
            totals = new List<Totals>();

        }

        //These variables are used in a few methods so they are global.
        private string fileName;
        private string directoryName;

    
        private double janValue;
        private  double febValue;
        private double marValue;
        private double aprValue;
        private double mayValue;
        private double junValue;
        private double julValue;
        private double augValue;
        private double septValue;
        private double octValue;
        private double novValue;
        private double decValue;

        private void mainPanel_Paint(object sender, PaintEventArgs e)
        {

         int width;
         double height;
            
      
            //This finds the actual pixel dimension of the panel, used for scaling
          width = mainPanel.ClientSize.Width-25; 
          height = mainPanel.ClientSize.Height-25;//-25 gives a boarder

            
           //Column width scaling code
          int padding = width / 13;
          int columnWidth=padding/3;
          double maxValue=0;
           //gets the maximum value from all those to be drawn
          getMaxValue(maxValue);

        //This makes the value a perctange factor of the panel height.
        double janDraw = (janValue / height) * 100;
        double febDraw = (febValue / height) * 100;
        double marDraw = (marValue / height) * 100;
        double aprDraw = (aprValue / height) * 100;
        double mayDraw = (mayValue / height) * 100;
        double junDraw = (junValue / height) * 100;
        double julDraw = (julValue / height) * 100;
        double augDraw = (augValue / height) * 100;
        double septDraw = (septValue / height) * 100;
        double octDraw = (octValue / height) * 100;
        double novDraw = (novValue / height) * 100;
        double decDraw = (decValue / height) * 100;

          

            //Columns to be used
            Rectangle jan;
            Rectangle feb;
            Rectangle mar;
            Rectangle apr;
            Rectangle may;
            Rectangle jun;
            Rectangle jul;
            Rectangle aug;
            Rectangle sep;
            Rectangle oct;
            Rectangle nov;
            Rectangle dec;



            Color solidColour = Color.Red;
            Color penColour = Color.Black;
            Brush solidBrush;
            Pen pen = new Pen(penColour, 1);

           // rectangle is given its dimension and position.
            jan = new Rectangle(padding, Convert.ToInt32((height) - janDraw), columnWidth, Convert.ToInt32(janDraw));
            feb = new Rectangle((padding * 2), Convert.ToInt32((height) - febDraw), columnWidth, Convert.ToInt32(febDraw));
            mar = new Rectangle((padding * 3), Convert.ToInt32((height) - marDraw), columnWidth, Convert.ToInt32(marDraw));
            apr = new Rectangle((padding * 4), Convert.ToInt32((height) - aprDraw), columnWidth, Convert.ToInt32(aprDraw));
            may = new Rectangle((padding * 5), Convert.ToInt32((height) - mayDraw), columnWidth, Convert.ToInt32(mayDraw));
            jun = new Rectangle((padding * 6), Convert.ToInt32((height) - junDraw), columnWidth, Convert.ToInt32(junDraw));
            jul = new Rectangle((padding * 7), Convert.ToInt32((height) - julDraw), columnWidth, Convert.ToInt32(julDraw));
            aug = new Rectangle((padding * 8), Convert.ToInt32((height) - augDraw), columnWidth, Convert.ToInt32(augDraw));
            sep = new Rectangle((padding * 9), Convert.ToInt32((height) - septDraw), columnWidth, Convert.ToInt32(septDraw));
            oct = new Rectangle((padding * 10), Convert.ToInt32((height) - octDraw), columnWidth, Convert.ToInt32(octDraw));
            nov = new Rectangle((padding * 11), Convert.ToInt32((height) - novDraw), columnWidth, Convert.ToInt32(novDraw));
            dec = new Rectangle((padding * 12), Convert.ToInt32((height) - decDraw), columnWidth, Convert.ToInt32(decDraw));
            
           
            using (Graphics main = mainPanel.CreateGraphics())
            using (solidBrush = new SolidBrush(solidColour))
            {
                //First two are lables for the bar, one will show the value above the bar, the other will show the month
                main.DrawString(janValue.ToString("0"), DefaultFont, solidBrush, padding, Convert.ToInt32(height-janDraw-20));
                main.DrawString("J", DefaultFont, solidBrush, padding, Convert.ToInt32(height));
                main.FillRectangle(solidBrush, jan);

                main.DrawString(febValue.ToString("0"), DefaultFont, solidBrush, padding*2, Convert.ToInt32(height - febDraw - 20));
                main.DrawString("F", DefaultFont, solidBrush, padding*2, Convert.ToInt32(height));
                main.FillRectangle(solidBrush, feb);

                main.DrawString(marValue.ToString("0"), DefaultFont, solidBrush, padding*3, Convert.ToInt32(height - marDraw - 20));
                main.DrawString("M", DefaultFont, solidBrush, padding * 3, Convert.ToInt32(height));
                main.FillRectangle(solidBrush, mar);

                main.DrawString(aprValue.ToString("0"), DefaultFont, solidBrush, padding*4, Convert.ToInt32(height - aprDraw - 20));
                main.DrawString("A", DefaultFont, solidBrush, padding * 4, Convert.ToInt32(height));
                main.FillRectangle(solidBrush, apr);

                main.DrawString(mayValue.ToString("0"), DefaultFont, solidBrush, padding*5, Convert.ToInt32(height - mayDraw - 20));
                main.DrawString("M", DefaultFont, solidBrush, padding * 5, Convert.ToInt32(height));
                main.FillRectangle(solidBrush, may);

                main.DrawString(junValue.ToString("0"), DefaultFont, solidBrush, padding*6, Convert.ToInt32(height - junDraw - 20));
                main.DrawString("J", DefaultFont, solidBrush, padding * 6, Convert.ToInt32(height));
                main.FillRectangle(solidBrush, jun);

                main.DrawString(julValue.ToString("0"), DefaultFont, solidBrush, padding*7, Convert.ToInt32(height - julDraw - 20));
                main.DrawString("J", DefaultFont, solidBrush, padding * 7, Convert.ToInt32(height));
                main.FillRectangle(solidBrush, jul);

                main.DrawString(augValue.ToString("0"), DefaultFont, solidBrush, padding*8, Convert.ToInt32(height - augDraw - 20));
                main.DrawString("A", DefaultFont, solidBrush, padding * 8, Convert.ToInt32(height));
                main.FillRectangle(solidBrush, aug);

                main.DrawString(septValue.ToString("0"), DefaultFont, solidBrush, padding*9, Convert.ToInt32(height - septDraw - 20));
                main.DrawString("S", DefaultFont, solidBrush, padding * 9, Convert.ToInt32(height));
                main.FillRectangle(solidBrush, sep);

                main.DrawString(octValue.ToString("0"), DefaultFont, solidBrush, padding*10, Convert.ToInt32(height - octDraw - 20));
                main.DrawString("O", DefaultFont, solidBrush, padding * 10, Convert.ToInt32(height));
                main.FillRectangle(solidBrush, oct);

                main.DrawString(novValue.ToString("0"), DefaultFont, solidBrush, padding*11, Convert.ToInt32(height - novDraw - 20));
                main.DrawString("N", DefaultFont, solidBrush, padding * 11, Convert.ToInt32(height));
                main.FillRectangle(solidBrush, nov);

                main.DrawString(decValue.ToString("0"), DefaultFont, solidBrush, padding*12, Convert.ToInt32(height - decDraw - 20));
                main.DrawString("D", DefaultFont, solidBrush, padding * 12, Convert.ToInt32(height));
                main.FillRectangle(solidBrush, dec);

                //Y axis lables and X/Y axis lines
                main.DrawLine(pen, 20, 10, 20, (int)height);
                main.DrawString(janValue.ToString("0"), DefaultFont, solidBrush, 0, Convert.ToInt32(height - janDraw));
                main.DrawString((octValue+50).ToString("0"), DefaultFont, solidBrush, 0, Convert.ToInt32(height-octDraw-50 ));
                main.DrawLine(pen, 20, (int)height, width, (int)height);

            }
        }


        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() != DialogResult.Cancel)
            {
                directoryName = openFileDialog.InitialDirectory;
                fileName = openFileDialog.FileName;
            }
            //Calls the method to be sorted 
            sortFile(fileName);
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            //Calls the method to be sorted, doing this code in the button could be limiting
            searchQuery();
          

        }
        /// <summary>
        /// SortFile method uses the filename as a parameter, with a try catch for file IO
        /// the method reads the file line by line and sorts it into columns which are stored 
        /// in the object list recordList
        /// </summary>
     
        public void sortFile(string fileName)
        {
            try
            {
                using (StreamReader reader = new StreamReader(fileName))
                {
                    string lineFromFile;
                    string[] col;
                    while ((lineFromFile = reader.ReadLine()) != null)
                    {
                        col = lineFromFile.Split(',');//separates values on each comma into columns
                        Record tempRecord;
                        tempRecord = new Record((col[0]), (col[1]), (col[2]), (col[3]), (col[4]), (col[5]), (col[6]));
                        recordList.Add(tempRecord);
                     

                    }
                }

                
                Record headers = recordList[0];//This removes the headers of the columns as they are not needed for record analysis.
                recordList.Remove(headers);

              
            }catch(IOException io)  //This will trigger typically if the file is open or being used by another application.
            {
              
            
                MessageBox.Show("Error reading from file, being used by another process?", "File Error",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
                Debug.WriteLine(io.Message);
                searchOutput.AppendText("No record found or file error");
            }
        
        }

       /// <summary>
       /// This is a basic method for finding the maximum value that is going to be outputted
       /// this is only used for the bar chart lables. The idea was to use this value along with
       /// the chart size as a scale factor.
       /// </summary>
        private double getMaxValue(double maxValue) {

            bool isMaxFound = false;
            while (isMaxFound ==false) {
                if (janValue > febValue)
                {
                    maxValue = janValue;
                }else { maxValue = febValue; }

                if (marValue > maxValue)
                {
                    maxValue = marValue;
                }

                if (aprValue > maxValue)
                {
                    maxValue = aprValue;
                }

                if (mayValue > maxValue)
                {
                    maxValue = mayValue;
                }

                if (junValue > maxValue)
                {
                    maxValue = junValue;
                }
                if
                    (julValue > maxValue)
                {
                    maxValue = julValue;
                }

                if (augValue > maxValue)
                {
                    maxValue = augValue;
                }

                if (septValue > maxValue)
                {
                    maxValue = septValue;
                }

                if (octValue > maxValue)
                {
                    maxValue = octValue;
                }

                if (novValue > maxValue)
                {
                    maxValue = novValue;
                }

                if (decValue > maxValue)
                {
                    maxValue = decValue;
                }
                else { isMaxFound = true; }
               
                
            }

return maxValue;
        }
        /// <summary>
        ///  This method is accessed when the user wants to search for a record
        ///  It takes the selected element (customer or invoice ID) and seaches for it in the apporiate list
        ///  once found, the rest of the information about the record is displayed from their apporiate lists.
        ///  This method has a formatexception catch.
        /// </summary>
        private void searchQuery()
        {
            //Used to produce and error if not found
            Boolean recordIsFound = false;
            string searchTerm = searchBox.Text;

            try
            {

                foreach (Record r in recordList)
                {

                    if (customerRadio.Checked)
                    {

                        if (searchTerm == r.CustomerID)
                        {
                            searchOutput.AppendText(Environment.NewLine);
                            searchOutput.AppendText("Record Found: ");
                            searchOutput.AppendText("Invoice ID: " + r.InvoiceID);
                            searchOutput.AppendText(Environment.NewLine);
                            searchOutput.AppendText("Stock Code: " + r.StockCode);
                            searchOutput.AppendText(Environment.NewLine);
                            searchOutput.AppendText("Description: " + r.Description);
                            searchOutput.AppendText(Environment.NewLine);
                            searchOutput.AppendText("Quantity: " + r.Quantity);
                            searchOutput.AppendText(Environment.NewLine);
                            searchOutput.AppendText("Purchase Date: " + r.InvoiceDate);
                            searchOutput.AppendText(Environment.NewLine);
                            searchOutput.AppendText("Unit Price: " + r.UnitPrice);
                            searchOutput.AppendText(Environment.NewLine);
                            searchOutput.AppendText("Customer ID: " + r.CustomerID);
                            searchOutput.AppendText(Environment.NewLine);
                            searchOutput.AppendText(Environment.NewLine);
                            recordIsFound = true;
                        }
                        else
                        {

                        }

                    }
                    else if (invoiceRadio.Checked)
                    {
                        if (r.InvoiceID == searchTerm)
                        {
                            searchOutput.AppendText(Environment.NewLine);
                            searchOutput.AppendText("Record Found: ");
                            searchOutput.AppendText("Invoice ID: " + r.InvoiceID);
                            searchOutput.AppendText(Environment.NewLine);
                            searchOutput.AppendText("Stock Code: " + r.StockCode);
                            searchOutput.AppendText(Environment.NewLine);
                            searchOutput.AppendText("Description: " + r.Description);
                            searchOutput.AppendText(Environment.NewLine);
                            searchOutput.AppendText("Quantity: " + r.Quantity);
                            searchOutput.AppendText(Environment.NewLine);
                            searchOutput.AppendText("Purchase Date: " + r.InvoiceDate);
                            searchOutput.AppendText(Environment.NewLine);
                            searchOutput.AppendText("Unit Price: " + r.UnitPrice);
                            searchOutput.AppendText(Environment.NewLine);
                            searchOutput.AppendText("Customer ID: " + r.CustomerID);
                            searchOutput.AppendText(Environment.NewLine);
                            searchOutput.AppendText(Environment.NewLine);
                            recordIsFound = true;
                        }

                    }
                }
            }
            catch (FormatException ex)
            {
                MessageBox.Show("Error reading from file", "File Error",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
                Debug.WriteLine(ex.Message);
                searchOutput.AppendText("No record found or file error");
            }

            if (recordIsFound == false)
            {
                searchOutput.AppendText("Record Not Found, ensure to open a csv file first and the correct search area is selected.");
                MessageBox.Show("Error Finding Record", "Record Error",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void graphDisplayButton_Click(object sender, EventArgs e)
        {
            //A combo box was decided to be the best approach as when using 
            //radio buttons they would trigger twice resulting in errors.
            if (graphComboBox.Text == "Total Quantity Sold")
            {
                //This calls the method in the calcualtions class with parameters
                Calculations.totalsAverages(recordList, totals);

                janValue = Convert.ToDouble(totals[0].totalQuantity);
                febValue = totals[1].totalQuantity;
                marValue = totals[2].totalQuantity;
                aprValue = totals[3].totalQuantity;
                mayValue = totals[4].totalQuantity;
                junValue = totals[5].totalQuantity;
                julValue = totals[6].totalQuantity;
                augValue = totals[7].totalQuantity;
                septValue = totals[8].totalQuantity;
                octValue = totals[9].totalQuantity;
                novValue = totals[10].totalQuantity;
                decValue = totals[11].totalQuantity;
                //Refreshes the panel, redraws
                mainPanel.Refresh();

            }
            else if (graphComboBox.Text == "Total Value Sold")
            {
                Calculations.totalsAverages(recordList, totals);
                janValue = (totals[0].spend);
                febValue = totals[1].spend;
                marValue = totals[2].spend;
                aprValue = totals[3].spend;
                mayValue = totals[4].spend;
                junValue = totals[5].spend;
                julValue = totals[6].spend;
                augValue = totals[7].spend;
                septValue = totals[8].spend;
                octValue = totals[9].spend;
                novValue = totals[10].spend;
                decValue = totals[11].spend;
                mainPanel.Refresh();
            }
            else if (graphComboBox.Text == "Invoices Generated")
            {
                Calculations.totalsAverages(recordList, totals);
                janValue = totals[0].totalInvoices;
                febValue = totals[1].totalInvoices;
                marValue = totals[2].totalInvoices;
                aprValue = totals[3].totalInvoices;
                mayValue = totals[4].totalInvoices;
                junValue = totals[5].totalInvoices;
                julValue = totals[6].totalInvoices;
                augValue = totals[7].totalInvoices;
                septValue = totals[8].totalInvoices;
                octValue = totals[9].totalInvoices;
                novValue = totals[10].totalInvoices;
                decValue = totals[11].totalInvoices;
                mainPanel.Refresh();

            }
            else if (graphComboBox.Text == "Average No. Items Per Invoice")
            {
                Calculations.totalsAverages(recordList, totals);
               
                janValue = totals[0].totalQuantity / totals[0].totalInvoices;
                febValue = totals[1].totalQuantity / totals[1].totalInvoices;
                marValue = totals[2].totalQuantity / totals[2].totalInvoices;
                aprValue = totals[3].totalQuantity / totals[3].totalInvoices;
                mayValue = totals[4].totalQuantity / totals[4].totalInvoices;
                junValue = totals[5].totalQuantity / totals[5].totalInvoices;
                julValue = totals[6].totalQuantity / totals[6].totalInvoices;
                augValue = totals[7].totalQuantity / totals[7].totalInvoices;
                septValue = totals[8].totalQuantity / totals[8].totalInvoices;
                octValue = totals[9].totalQuantity / totals[9].totalInvoices;
                novValue = totals[10].totalQuantity / totals[10].totalInvoices;
                decValue = totals[11].totalQuantity / totals[11].totalInvoices;
                mainPanel.Refresh();
            }
            else if (graphComboBox.Text == "Average Spend Per Customer")
            {
                //As values from both method are needed to be calculated both methods
                //have to be called in order for a value to be generated
                Calculations.totalsAverages(recordList, totals);
                Calculations.customerAverages(recordList, totals);            
                janValue = totals[0].averageCustomerSpend;
                febValue = totals[1].averageCustomerSpend;
                marValue = totals[2].averageCustomerSpend;
                aprValue = totals[3].averageCustomerSpend;
                mayValue = totals[4].averageCustomerSpend;
                junValue = totals[5].averageCustomerSpend;
                julValue = totals[6].averageCustomerSpend;
                augValue = totals[7].averageCustomerSpend;
                septValue = totals[8].averageCustomerSpend;
                octValue = totals[9].averageCustomerSpend;
                novValue = totals[10].averageCustomerSpend;
                decValue = totals[11].averageCustomerSpend;
                mainPanel.Refresh();

            }
            else if (graphComboBox.Text == "Average Spend Per Invoice")
            {
                Calculations.totalsAverages(recordList, totals);
                janValue = totals[0].avSpend;                
                febValue = totals[1].avSpend;               
                marValue = totals[2].avSpend;                
                aprValue = totals[3].avSpend;               
                mayValue = totals[4].avSpend;               
                junValue = totals[5].avSpend;               
                julValue = totals[6].avSpend;             
                augValue = totals[7].avSpend;
                septValue = totals[8].avSpend;
                octValue = totals[9].avSpend;                       
                novValue = totals[10].avSpend;
                decValue = totals[11].avSpend;                       
                mainPanel.Refresh();
               
            }
            else if (graphComboBox.Text == "Unique Customers") {
                Calculations.totalsAverages(recordList, totals);
                Calculations.customerAverages(recordList, totals);

                janValue = Convert.ToDouble(totals[0].uniqueCustomerAmount);
                febValue = totals[1].uniqueCustomerAmount;
                marValue = totals[2].uniqueCustomerAmount;
                aprValue = totals[3].uniqueCustomerAmount;
                mayValue = totals[4].uniqueCustomerAmount;
                junValue = totals[5].uniqueCustomerAmount;
                julValue = totals[6].uniqueCustomerAmount;
                augValue = totals[7].uniqueCustomerAmount;
                septValue = totals[8].uniqueCustomerAmount;
                octValue = totals[9].uniqueCustomerAmount;
                novValue = totals[10].uniqueCustomerAmount;
                decValue = totals[11].uniqueCustomerAmount;
                mainPanel.Refresh();
            }
        }

        private void redrawButton_Click(object sender, EventArgs e)
        {
            //Redraws the panel if the user wishes
            mainPanel.Refresh();
        }
    }
}


   
