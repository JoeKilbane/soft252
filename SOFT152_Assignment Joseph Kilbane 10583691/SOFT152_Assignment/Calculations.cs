﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SOFT152_Assignment
{
    class Calculations
    {
        //list declarations
        private List<Record> recordList;

        private List<Totals> totals;


        public Calculations()
        {
            recordList = new List<Record>();
            totals = new List<Totals>();
        }
/// <summary>
/// This method is one of the main methods in the program, it find out every value needed
/// apart from the unique customers. This method has to be set to public and static in
/// order for it to be called with parameters across classes. This method calls in both
/// object lists.
/// </summary>

        public static void totalsAverages(List<Record> recordList, List<Totals> totals)
        {
            //This should be improved upon but all variables are needed in the current format.
            int janQuan = 0;
            int febQuan = 0;
            int marQuan = 0;
            int aprQuan = 0;
            int mayQuan = 0;
            int junQuan = 0;
            int julQuan = 0;
            int augQuan = 0;
            int septQuan = 0;
            int octQuan = 0;
            int novQuan = 0;
            int decQuan = 0;

            int janInvoices = 0;
            int febInvoices = 0;
            int marInvoices = 0;
            int aprInvoices = 0;
            int mayInvoices = 0;
            int junInvoices = 0;
            int julInvoices = 0;
            int augInvoices = 0;
            int octInvoices = 0;
            int septInvoices = 0;
            int novInvoices = 0;
            int decInvoices = 0;


            double janAvSpend = 0.0;
            double febAvSpend = 0.0;
            double marAvSpend = 0.0;
            double aprAvSpend = 0.0;
            double mayAvSpend = 0.0;
            double junAvSpend = 0.0;
            double julAvSpend = 0.0;
            double augAvSpend = 0.0;
            double septAvSpend = 0.0;
            double octAvSpend = 0.0;
            double novAvSpend = 0.0;
            double decAvSpend = 0.0;


            double janSpend = 0.0;
            double febSpend = 0.0;
            double marSpend = 0.0;
            double aprSpend = 0.0;
            double maySpend = 0.0;
            double junSpend = 0.0;
            double julSpend = 0.0;
            double augSpend = 0.0;
            double septSpend = 0.0;
            double octSpend = 0.0;
            double novSpend = 0.0;
            double decSpend = 0.0;



            string[] dateSplit;     //split invoices into months
            char[] delimator = { '/' };//dates are split with forward slash


            string day;  //day and year are not needed, but could be used for future expansion
            string month;
            string year;

            Totals tempTotal;
try
                {
            foreach (Record r in recordList)
            {
                
                    dateSplit = r.InvoiceDate.Split(delimator);
                    day = (dateSplit[0]);
                    month = (dateSplit[1]);
                    year = (dateSplit[2]);
                    //check varibles
             
                switch (month)
                {
                    case "01"://Signifies january as the month is in string format.
                        {
                            janQuan = janQuan + (Convert.ToInt32(r.Quantity));//Adds record quantity to the running total of quantity
                            //Adds record spend (quantity*price) to a running total
                            janSpend = janSpend + (Convert.ToDouble(r.Quantity) * Convert.ToDouble(r.UnitPrice));
                            janInvoices++;//increments invoice number

                            break;
                        }
                    case "02":
                        {
                            febQuan = febQuan + (Convert.ToInt32(r.Quantity));
                            febSpend = febSpend + (Convert.ToDouble(r.Quantity) * Convert.ToDouble(r.UnitPrice));
                            febInvoices++;

                            break;
                        }
                    case "03":
                        {
                            marQuan = marQuan + (Convert.ToInt32(r.Quantity));
                            marSpend = marSpend + (Convert.ToDouble(r.Quantity) * Convert.ToDouble(r.UnitPrice));
                            marInvoices++;

                            break;
                        }
                    case "04":
                        {
                            aprQuan = aprQuan + (Convert.ToInt32(r.Quantity));
                            aprSpend = aprSpend + (Convert.ToDouble(r.Quantity) * Convert.ToDouble(r.UnitPrice));
                            aprInvoices++;

                            break;
                        }
                    case "05":
                        {
                            mayQuan = mayQuan + (Convert.ToInt32(r.Quantity));
                            maySpend = maySpend + (Convert.ToDouble(r.Quantity) * Convert.ToDouble(r.UnitPrice));
                            mayInvoices++;

                            break;
                        }
                    case "06":
                        {
                            junQuan = junQuan + (Convert.ToInt32(r.Quantity));
                            junSpend = junSpend + (Convert.ToDouble(r.Quantity) * Convert.ToDouble(r.UnitPrice));
                            junInvoices++;

                            break;
                        }
                    case "07":
                        {
                            julQuan = julQuan + (Convert.ToInt32(r.Quantity));
                            julSpend = julSpend + (Convert.ToDouble(r.Quantity) * Convert.ToDouble(r.UnitPrice));
                            julInvoices++;
                            break;
                        }
                    case "08":
                        {
                            augQuan = augQuan + (Convert.ToInt32(r.Quantity));
                            augSpend = augSpend + (Convert.ToDouble(r.Quantity) * Convert.ToDouble(r.UnitPrice));
                            augInvoices++;
                            break;
                        }
                    case "09":
                        {
                            septQuan = septQuan + (Convert.ToInt32(r.Quantity));
                            septSpend = septSpend + (Convert.ToDouble(r.Quantity) * Convert.ToDouble(r.UnitPrice));
                            septInvoices++;
                            break;
                        }
                    case "10":
                        {
                            octQuan = octQuan + (Convert.ToInt32(r.Quantity));
                            octSpend = octSpend + (Convert.ToDouble(r.Quantity) * Convert.ToDouble(r.UnitPrice));
                            octInvoices++;
                            break;
                        }
                    case "11":
                        {
                            novQuan = novQuan + (Convert.ToInt32(r.Quantity));
                            novSpend = novSpend + (Convert.ToDouble(r.Quantity) * Convert.ToDouble(r.UnitPrice));
                            novInvoices++;
                            break;
                        }
                    case "12":
                        {
                            decQuan = decQuan + (Convert.ToInt32(r.Quantity));
                            decSpend = decSpend + (Convert.ToDouble(r.Quantity) * Convert.ToDouble(r.UnitPrice));
                            decInvoices++;
                            break;
                        }


                }


            }
   }
                catch(IndexOutOfRangeException) {
                MessageBox.Show("Error Reading Date. Please load file again to fix", "File Error",
               MessageBoxButtons.OK, MessageBoxIcon.Error);
               
            }
            janAvSpend = janSpend / janInvoices;//Calculates the average spend per invoice
            febAvSpend = febSpend / febInvoices;
            marAvSpend = marSpend / marInvoices;
            aprAvSpend = aprSpend / aprInvoices;
            mayAvSpend = maySpend / mayInvoices;
            junAvSpend = junSpend / junInvoices;
            julAvSpend = julSpend / julInvoices;
            augAvSpend = augSpend / augInvoices;
            septAvSpend = septSpend / septInvoices;
            octAvSpend = octSpend / octInvoices;
            novAvSpend = novSpend / novInvoices;
            decAvSpend = decSpend / decInvoices;


            //Calculated values being added to the object list
            //a temp total is made so they can be inserted 
            tempTotal = new Totals(janInvoices, janAvSpend, janQuan, janSpend);
            //inserts values, this cannot be add as then the customer values won't appear
            totals.Insert(0,tempTotal);

            tempTotal = new Totals(febInvoices, febAvSpend, febQuan, febSpend);
            totals.Insert(1, tempTotal);

            tempTotal = new Totals(marInvoices, marAvSpend, marQuan, marSpend);
            totals.Insert(2, tempTotal);

            tempTotal = new Totals(aprInvoices, aprAvSpend, aprQuan, aprSpend);
            totals.Insert(3, tempTotal);

            tempTotal = new Totals(mayInvoices, mayAvSpend, mayQuan, maySpend);
            totals.Insert(4, tempTotal);

            tempTotal = new Totals(junInvoices, junAvSpend, junQuan, junSpend);
            totals.Insert(5, tempTotal);

            tempTotal = new Totals(julInvoices, julAvSpend, julQuan, julSpend);
            totals.Insert(6, tempTotal);

            tempTotal = new Totals(augInvoices, augAvSpend, augQuan, augSpend);
            totals.Insert(7, tempTotal);

            tempTotal = new Totals(septInvoices, septAvSpend, septQuan, septSpend);
            totals.Insert(8, tempTotal);

            tempTotal = new Totals(octInvoices, octAvSpend, octQuan, octSpend);
            totals.Insert(9, tempTotal);

            tempTotal = new Totals(novInvoices, novAvSpend, novQuan, novSpend);
            totals.Insert(10, tempTotal);

            tempTotal = new Totals(decInvoices, decAvSpend, decQuan, decSpend);
            totals.Insert(11, tempTotal);

          


        }

        /// <summary>
        /// This is one of the other main methods, it also has to be public and static
        /// in order to be access across classes. Using the lists as parameters this method 
        /// finds the number of unique customers per month and the customer ID of those customers
        /// while removing any duplicates
        /// </summary>
     
        public static void customerAverages(List<Record> recordList, List<Totals> totals)
        {
            int janCustomer = 0;
            int febCustomer = 0;
            int marCustomer = 0;
            int aprCustomer = 0;
            int mayCustomer = 0;
            int junCustomer = 0;
            int julCustomer = 0;
            int augCustomer = 0;
            int septCustomer = 0;
            int octCustomer = 0;
            int novCustomer = 0;
            int decCustomer = 0;

            //Lists used to store the unique customer ID
            List<string> uniqueCustomersJan = new List<string>();
            List<string> uniqueCustomersFeb = new List<string>();
            List<string> uniqueCustomersMar = new List<string>();
            List<string> uniqueCustomersApr = new List<string>();
            List<string> uniqueCustomersMay = new List<string>();
            List<string> uniqueCustomersJun = new List<string>();
            List<string> uniqueCustomersJul = new List<string>();
            List<string> uniqueCustomersAug = new List<string>();
            List<string> uniqueCustomersSept = new List<string>();
            List<string> uniqueCustomersOct = new List<string>();
            List<string> uniqueCustomersNov = new List<string>();
            List<string> uniqueCustomersDec = new List<string>();



            string[] dateSplit;     //split invoices into months
            char[] delimator = { '/' };//dates are split with forward slash


            string day;  //day and year are not needed, but could be used for future expansion
            string month;
            string year;

            Totals tempTotal;
            try
            {
                foreach (Record r in recordList)
                {
                    dateSplit = r.InvoiceDate.Split(delimator);
                    day = (dateSplit[0]);
                    month = (dateSplit[1]);
                    year = (dateSplit[2]);

                    bool isUnique = true;
                    switch (month)
                    {
                        case "01":
                            {


                                // Used to store the current ID being passed in
                                string testCustomerID = r.CustomerID;

                                // Loops the list of unique customers for this month
                                for (int i = 0; i < uniqueCustomersJan.Count; i++)
                                {
                                    //If the ID matches any of the IDs in the list
                                    // it means it has already been added, therefore not unique.
                                    if (testCustomerID == uniqueCustomersJan[i])
                                    {
                                        isUnique = false;
                                    }
                                }
                                //If the boolean remains true then the value is unique
                                // it then gets added to the list and the number of customers
                                //for the month increases
                                if (isUnique == true)
                                {
                                    janCustomer++;
                                    uniqueCustomersJan.Add(testCustomerID);//ID is then added to the list of uniques

                                }
                            }

                            break;

                        case "02":
                            {


                                string testCustomerID = r.CustomerID;


                                for (int i = 0; i < uniqueCustomersFeb.Count; i++)
                                {

                                    if (testCustomerID == uniqueCustomersFeb[i])
                                    {
                                        isUnique = false;
                                    }
                                }

                                if (isUnique == true)
                                {
                                    febCustomer++;
                                    uniqueCustomersFeb.Add(testCustomerID);
                                }

                                break;
                            }
                        case "03":
                            {


                                string testCustomerID = r.CustomerID;

                                for (int i = 0; i < uniqueCustomersMar.Count; i++)
                                {

                                    if (testCustomerID == uniqueCustomersMar[i])
                                    {
                                        isUnique = false;
                                    }
                                }

                                if (isUnique == true)
                                {
                                    marCustomer++;
                                    uniqueCustomersMar.Add(testCustomerID);

                                }

                                break;
                            }
                        case "04":
                            {


                                string testCustomerID = r.CustomerID;

                                for (int i = 0; i < uniqueCustomersApr.Count; i++)
                                {

                                    if (testCustomerID == uniqueCustomersApr[i])
                                    {
                                        isUnique = false;
                                    }
                                }

                                if (isUnique == true)
                                {
                                    aprCustomer++;
                                    uniqueCustomersApr.Add(testCustomerID);

                                }

                                break;
                            }
                        case "05":
                            {


                                string testCustomerID = r.CustomerID;

                                for (int i = 0; i < uniqueCustomersMay.Count; i++)
                                {

                                    if (testCustomerID == uniqueCustomersMay[i])
                                    {
                                        isUnique = false;
                                    }

                                }

                                if (isUnique == true)
                                {
                                    mayCustomer++;
                                    uniqueCustomersMay.Add(testCustomerID);

                                }

                                break;
                            }
                        case "06":
                            {


                                string testCustomerID = r.CustomerID;

                                for (int i = 0; i < uniqueCustomersJun.Count; i++)
                                {

                                    if (testCustomerID == uniqueCustomersJun[i])
                                    {
                                        isUnique = false;
                                    }
                                }

                                if (isUnique == true)
                                {
                                    junCustomer++;
                                    uniqueCustomersJun.Add(testCustomerID);

                                }

                                break;
                            }
                        case "07":
                            {

                                string testCustomerID = r.CustomerID;

                                for (int i = 0; i < uniqueCustomersJul.Count; i++)
                                {

                                    if (testCustomerID == uniqueCustomersJul[i])
                                    {
                                        isUnique = false;
                                    }
                                }

                                if (isUnique == true)
                                {
                                    julCustomer++;
                                    uniqueCustomersJul.Add(testCustomerID);

                                }
                                break;
                            }
                        case "08":
                            {


                                string testCustomerID = r.CustomerID;

                                for (int i = 0; i < uniqueCustomersAug.Count; i++)
                                {

                                    if (testCustomerID == uniqueCustomersAug[i])
                                    {
                                        isUnique = false;
                                    }
                                }

                                if (isUnique == true)
                                {
                                    augCustomer++;
                                    uniqueCustomersAug.Add(testCustomerID);

                                }
                                break;
                            }
                        case "09":
                            {

                                string testCustomerID = r.CustomerID;

                                for (int i = 0; i < uniqueCustomersSept.Count; i++)
                                {

                                    if (testCustomerID == uniqueCustomersSept[i])
                                    {
                                        isUnique = false;
                                    }
                                }

                                if (isUnique == true)
                                {
                                    septCustomer++;
                                    uniqueCustomersSept.Add(testCustomerID);

                                }
                                break;
                            }
                        case "10":
                            {

                                string testCustomerID = r.CustomerID;

                                for (int i = 0; i < uniqueCustomersOct.Count; i++)
                                {

                                    if (testCustomerID == uniqueCustomersOct[i])
                                    {
                                        isUnique = false;
                                    }
                                }

                                if (isUnique == true)
                                {
                                    octCustomer++;
                                    uniqueCustomersOct.Add(testCustomerID);

                                }
                                break;
                            }
                        case "11":
                            {


                                string testCustomerID = r.CustomerID;

                                for (int i = 0; i < uniqueCustomersNov.Count; i++)
                                {

                                    if (testCustomerID == uniqueCustomersNov[i])
                                    {
                                        isUnique = false;
                                    }
                                }

                                if (isUnique == true)
                                {
                                    novCustomer++;
                                    uniqueCustomersNov.Add(testCustomerID);
                                }

                                break;
                            }
                        case "12":
                            {

                                string testCustomerID = r.CustomerID;

                                for (int i = 0; i < uniqueCustomersDec.Count; i++)
                                {

                                    if (testCustomerID == uniqueCustomersDec[i])
                                    {
                                        isUnique = false;
                                    }
                                }

                                if (isUnique == true)
                                {
                                    decCustomer++;
                                    uniqueCustomersDec.Add(testCustomerID);

                                }
                                break;
                            }
                    }

                }
            }
            catch (IndexOutOfRangeException) {
                MessageBox.Show("Error Reading Date. Please load file again to fix", "File Error",
             MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

//This finds the average customer spend per month

            double janCustomerAverage;
            janCustomerAverage = totals[0].spend / Convert.ToDouble(janCustomer);

            double febCustomerAverage;
            febCustomerAverage = totals[1].spend / febCustomer;

            double marCustomerAverage;
            marCustomerAverage = totals[2].spend / aprCustomer;

            double aprCustomerAverage;
            aprCustomerAverage = totals[3].spend / marCustomer;

            double mayCustomerAverage;
            mayCustomerAverage = totals[4].spend / mayCustomer;

            double junCustomerAverage;
            junCustomerAverage = totals[5].spend / junCustomer;

            double julCustomerAverage;
            julCustomerAverage = totals[6].spend / julCustomer;

            double augCustomerAverage;
            augCustomerAverage = totals[7].spend / augCustomer;

            double septCustomerAverage;
            septCustomerAverage = totals[8].spend / septCustomer;

            double octCustomerAverage;
            octCustomerAverage = totals[9].spend / octCustomer;

            double novCustomerAverage;
            novCustomerAverage = totals[10].spend / novCustomer;

            double DecCustomerAverage;
            DecCustomerAverage = totals[11].spend / decCustomer;



            //Adds to totals, Each index hold a months of calculations 
            tempTotal = new Totals( janCustomer,janCustomerAverage);
            totals.Insert(0,tempTotal);

            tempTotal = new Totals( febCustomer,febCustomerAverage);
            totals.Insert(1, tempTotal);

            tempTotal = new Totals( marCustomer,marCustomerAverage);
            totals.Insert(2, tempTotal);

            tempTotal = new Totals(aprCustomer,aprCustomerAverage);
            totals.Insert(3, tempTotal);

            tempTotal = new Totals(mayCustomer,mayCustomerAverage);
            totals.Insert(4, tempTotal);

            tempTotal = new Totals(junCustomer,junCustomerAverage);
            totals.Insert(5, tempTotal);

            tempTotal = new Totals(julCustomer,julCustomerAverage);
            totals.Insert(6, tempTotal);

            tempTotal = new Totals(augCustomer,augCustomerAverage);
            totals.Insert(7, tempTotal);

            tempTotal = new Totals(septCustomer,septCustomerAverage);
            totals.Insert(8, tempTotal);

            tempTotal = new Totals(octCustomer,octCustomerAverage);
            totals.Insert(9, tempTotal);

            tempTotal = new Totals(novCustomer,novCustomerAverage);
            totals.Insert(10, tempTotal);

            tempTotal = new Totals(decCustomer,DecCustomerAverage);
            totals.Insert(11, tempTotal);
        }

       
    }
}
