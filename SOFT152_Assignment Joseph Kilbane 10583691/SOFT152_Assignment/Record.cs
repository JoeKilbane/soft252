﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Threading.Tasks;

namespace SOFT152_Assignment
{
    class Record
    {
       
        //Variables for object Record
        private string invoiceID;
        private string stockCode;
        private string description;
        private string quantity;
        private string invoiceDate;
        private string unitPrice;
        private string customerID;


        

   

       //Constructor for records
        public Record(string InvoiceID, string StockCode, string Description, string Quantity, string InvoiceDate, string UnitPrice, string CustomerID)
        {
            invoiceID = InvoiceID;
           stockCode = StockCode;
           description = Description;
            quantity = Quantity;
          invoiceDate = InvoiceDate;
         unitPrice = UnitPrice;
           customerID = CustomerID;
        }

       //Gets and sets are needed as these properties are written to and read from.
        public string InvoiceID { set { invoiceID = value; } get { return invoiceID; } }
        public string StockCode {  set { stockCode = value; } get { return stockCode; } }
        public string Description {  set { description = value; } get { return description; } }
        public string Quantity {  set { quantity = value; } get { return quantity; } }
        public string InvoiceDate {  set { invoiceDate = value; } get { return invoiceDate; } }
        public string UnitPrice {  set { unitPrice = value; } get { return unitPrice; } }
        public string CustomerID {  set { customerID = value; } get { return customerID; } }



    }
}
