/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atm;

/**
 *
 * @author jpkilbane
 */
public class Card implements IState {

 
    @Override
    public void insertCard(ATM atm) {
        System.out.println("Going to PIN mode");
      atm.setState(new Pin());
    }

    @Override
    public void eject(ATM atm) {
  System.out.println("Ejecting Card");
      atm.setState(new Idle());
    }

   

    @Override
    public void amount(ATM atm) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void refill(ATM atm) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
