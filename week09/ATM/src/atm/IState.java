/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atm;

/**
 *
 * @author jpkilbane
 */
public interface IState {
  public void insertCard(ATM atm);
  public void eject(ATM atm);
  public void amount(ATM atm);
  public void refill(ATM atm);
}


